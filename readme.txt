1. Each person modifies their xml under their name folder, and write necessary report in .txt file

2. Don't modify teammates' folder, do anything you want in your folder

3. At least commit once after you finish one website

4.  Here are explanations of each file
    ./name/name.xml: each person's job
    ./name/name.txt: each person's report
    ./plan.txt: define the schedules and project plans
    ./git.txt: lists some git tips
    ./feedback.txt: everyone writes their feedbacks here
    ./detail.txt: detail to know
    ./description.txt: explanation of each field in xml
    ./cheatsheet.pdf: xpath cheatsheet

